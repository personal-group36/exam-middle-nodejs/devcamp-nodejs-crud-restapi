// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import car controller
const carController = require("../controllers/carController");

router.get("/cars", carController.getAllCar);

router.post("/users/:userId/cars", carController.createCar);

router.get("/users/:userId/cars", carController.getAllCarOfUser);

router.get("/cars/:carId", carController.getCarById);

router.put("/cars/:carId", carController.updateCarById);

router.delete("/courses/:courseId/cars/:carId", carController.deleteCar);

module.exports = router;