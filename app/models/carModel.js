const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const carSchema = new Schema({
    model:{
        type: String,
        required: true
    },
    vId:{
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Car", carSchema)