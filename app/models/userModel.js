const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    phone:{
        type: String,
        required: true,
        unique: true
    },
    age:{
        type: Number,
        default: 0
    },
    cars:[{
        type: Schema.Types.ObjectId,
        ref: "Car"
    }]
}, {
    timestamps: true
})

module.exports = mongoose.model("User", userSchema)