const mongoose = require("mongoose");

const carModel = require("../models/carModel");
const userModel = require("../models/userModel");

const createCar = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;
    console.log(userId);
    console.log(body);

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    if (!body.model !== undefined && body.model.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "model không hợp lệ"
        })
    }
    if (!body.vId !== undefined && body.vId.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "vId không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    const newCar = {
        _id: mongoose.Types.ObjectId(),
        model: body.model,
        vId: body.vId
    }
    console.log(newCar);

    carModel.create(newCar, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Thên ID của car mới được tạo vào trong mảng cars của user truyền tại params
        userModel.findByIdAndUpdate(userId, {
            $push: {
                cars: data._id
            }
        }, (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create car successfully",
                data: data
            })
        })
    })
}

const getCarById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const carId = request.params.carId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CarId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    carModel.findById(carId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail car successfully",
            data: data
        })
    })
}

const updateCarById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const carId = request.params.carId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CarID không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Id không hợp lệ"
        })
    }

    if (!body.model !== undefined && body.model.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "model không hợp lệ"
        })
    }
    if (!body.vId !== undefined && body.vId.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "vId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateCar = {}

    if(body.model !== undefined) {
        updateCar.model = body.model
    }

    if(body.vId !== undefined) {
        updateCar.vId = body.vId
    }

    carModel.findByIdAndUpdate(carId, updateCar, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update car successfully",
            data: updateCar
        })
    })
}

const getAllCar = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    carModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all cars successfully",
            data: data
        })
    })
}

const getAllCarOfUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findById(userId)
        .populate("cars")
        .exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
    
            return response.status(200).json({
                status: "Get all cars of user successfully",
                data: data
            })
        })
}

const deleteCar = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const carId = request.params.carId;
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CarId không hợp lệ"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    carModel.findByIdAndDelete(carId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        userModel.findByIdAndUpdate(userId, {
           $pull: { cars: carId } 
        }, (err) => {
            if(err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(200).json({
                status: "Delete car successfully"
            })
        })
    })
}

module.exports = {
    createCar,
    getCarById,
    updateCarById,
    getAllCar,
    getAllCarOfUser,
    deleteCar
}