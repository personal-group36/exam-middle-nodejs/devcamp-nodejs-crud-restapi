// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module User Model
const userModel = require("../models/userModel");

const getAllUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    userModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all user successfully",
            data: data
        })
    })
}

const createUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    console.log(body);
    // B2: Validate dữ liệu
    // Kiểm tra name có hợp lệ hay không
    if(!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
    // Kiểm tra phone có hợp lệ hay không
    if(!body.phone) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        phone: body.phone,
        age: body.age,
        cars: body.cars,
    }

    userModel.create(newUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create user successfully",
            data: data
        })
    })
}

const getUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findById(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail user successfully",
            data: data
        })
    })
}

const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserID không hợp lệ"
        })
    }
    // Kiểm tra name có hợp lệ hay không
    if(!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
    // Kiểm tra phone có hợp lệ hay không
    if(!body.phone) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const updateUser = {}

    if(body.name !== undefined) {
        updateUser.name = body.name
    }

    if(body.phone !== undefined) {
        updateUser.phone = body.phone
    }

    if(body.age !== undefined) {
        updateUser.age = body.age
    }
    
    if(body.age !== undefined) {
        updateUser.age = body.age
    }

    if(body.cars !== undefined) {
        updateUser.cars = body.cars
    }

    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update user successfully",
            data: updateUser
        })
    })
}

const deleteUserByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete user successfully"
        })
    })
}

module.exports = {
    getAllUser,
    createUser,
    getUserById,
    updateUserById,
    deleteUserByID
}