// khai báo thư viện cần dùng
const express = require("express");
const app = express();
const mongoose = require("mongoose")

// Cấu hình request đọc được body json
app.use(express.json());

// Khai báo router app
const userRouter = require("./app/routes/userRouter");
const carRouter = require("./app/routes/carRouter");

// App sử dụng router
app.use(userRouter);
app.use(carRouter);

// kết nối với csdl
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_RestAPI", (error) => {
    if(error) throw error;
    console.log("Connect MongoDB successfully!");
})
// khai báo cổng
const port = 8000;

// Chạy app trên cổng
app.listen(port, function() {
    console.log(`App listening on port ${port}`);
})